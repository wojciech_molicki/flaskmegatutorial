# coding: utf-8
from flask.ext.wtf import Form

from wtforms import TextField, BooleanField, TextAreaField, FileField, IntegerField, FloatField
from wtforms.validators import Required, Length, Optional

from app.models import User


class LoginForm(Form):
    # Required() is a validator, it checks if the field is not empty
    openid = TextField('openid', validators=[Required()])
    remember_me = BooleanField('remember_me', default=False)


class EditForm(Form):
    nickname = TextField('nickname', validators=[Required()])
    about_me = TextAreaField('about_me', validators=[Length(min=0, max=255)])
    lon = FloatField(u'Długość geograficzna', validators=[Optional()])
    lat = FloatField(u'Szerokość geograficzna', validators=[Optional()])

    def __init__(self, original_nickname, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_nickname = original_nickname

    def validate(self):
        if not Form.validate(self):
            return False
        if self.nickname.data == self.original_nickname:
            return True
        user = User.query.filter_by(nickname=self.nickname.data).first()
        if user != None:
            self.nickname.errors.append('This nickname is already in use. Please choose another one.')
            return False
        return True


class PostForm(Form):
    post = TextField('post', validators=[Required()])


class SearchForm(Form):
    search = TextField('search', validators=[Required()])


class UploadForm(Form):
    image = FileField(u'Zdjęcie karty')
    name = TextField('Nazwa')
    description = TextAreaField(u'Opis')
    quantity = IntegerField(u'Ilość', default=1)
    price = FloatField(u'Cena')


class CardEditForm(Form):
    quantity = IntegerField(u'Ilość', validators=[Optional()])
    delete = BooleanField(u'Usuń')


class CardReserveForm(Form):
    reserve = IntegerField(u'Rezerwuj', validators=[Optional()])
    unreserve = BooleanField(u'Zwolnij', default=False)
