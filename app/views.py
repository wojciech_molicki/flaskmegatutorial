# coding: utf-8
from datetime import datetime

from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required

from config import POSTS_PER_PAGE, MAX_SEARCH_RESULTS, ALLOWED_EXTENSIONS

from app import app, db, lm, oid
from forms import LoginForm, EditForm, PostForm, SearchForm, CardReserveForm, CardEditForm
from models import User, ROLE_USER, ROLE_ADMIN, Post, Card
from emails import follower_notification
from util import random_string

import os
from werkzeug.utils import secure_filename

from flask_oauth import OAuth

oauth = OAuth()
twitter = oauth.remote_app('twitter',
                           base_url='https://api.twitter.com/1/',
                           request_token_url='https://api.twitter.com/oauth/request_token',
                           access_token_url='https://api.twitter.com/oauth/access_token',
                           authorize_url='https://api.twitter.com/oauth/authenticate',
                           consumer_key='Nd0fQB8QOnzBaavWUqwGBa2mQ',
                           consumer_secret='Kj3pQQY7ftkQwlQVURQC1e53rZiCxqwT0x7FzoTfG3n8ha2LlQ'
                           )

facebook = oauth.remote_app('facebook',
                            base_url='https://graph.facebook.com/',
                            request_token_url=None,
                            access_token_url='/oauth/access_token',
                            authorize_url='https://www.facebook.com/dialog/oauth',
                            consumer_key='700352870018957',
                            consumer_secret='0a7e0b65c6239393f7f5e5f327bd48ed',
                            request_token_params={'scope': 'email'}
                            )


@twitter.tokengetter
def get_twitter_token(token=None):
    return session.get('twitter_token')


@facebook.tokengetter
def get_facebook_token(token=None):
    return session.get('facebook_token')


@app.route('/oauth-authorized')
@twitter.authorized_handler
def oauth_authorized(resp):
    next_url = request.args.get('next') or url_for('index')
    if resp is None:
        flash(u'You denied the request to sign in.')
        return redirect(next_url)

    session['twitter_token'] = (
        resp['oauth_token'],
        resp['oauth_token_secret']
    )
    session['twitter_user'] = resp['screen_name']

    session['facebook_token'] = (
        resp['oauth_token'],
        resp['oauth_token_secret']
    )
    session['facebook_user'] = resp['screen_name']

    flash('You were signed in as %s' % resp['screen_name'])
    return redirect(next_url)


@app.route('/login/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    next_url = request.args.get('next') or url_for('index')
    if resp is None or 'access_token' not in resp:
        return redirect(next_url)

    session['logged_in'] = True
    flash('You are logged in with Facebook.')
    session['facebook_token'] = (resp['access_token'], '')

    return redirect(next_url)


@app.route('/login/twitter')
def login_twitter():
    return twitter.authorize(callback=url_for('oauth_authorized',
                                              next=request.args.get('next') or request.referrer or None))


@app.route('/login/facebook')
def login_facebook():
    return facebook.authorize(callback=url_for('facebook_authorized',
                                               next=request.args.get('next'), _external=True))


# function decorators maps url's to functions (in this case - two urls)
# pagination support - index takes default parameter (int)
@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/index/<int:page>', methods=['GET', 'POST'])
@login_required
def index(page=1):
    form = PostForm()

    if form.validate_on_submit():
        post = Post(body=form.post.data, date=datetime.utcnow(), author=g.user)
        print post
        db.session.add(post)
        db.session.commit()
        flash('Your post is now live!')

        # redirect to index, to prevent inserting duplicates after user hits refresh
        # in browser
        return redirect(url_for('index'))

    # paginate takes the number to display, max POSTS_PER_PAGE, and behavour when
    # overflown (when page > POSTS_PER_PAGE) and returns shorter list
    posts = g.user.followed_posts().paginate(page, POSTS_PER_PAGE, False)

    return render_template("index.html",
                           title='Home page',
                           form=form,
                           posts=posts)


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


# decorators : can be used to always check if user is logged in
# instead of checking authentication in every method, just put
# @login_required at the top of definitions

# any functions decorated with before_request will run before 
# the view function each time a request is recieved.

@app.before_request
def before_request():
    # current_user is set by Flask-Login, we copy it to g.user
    # so now all requests will have access to logged in user
    g.user = current_user

    if g.user.is_authenticated:
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()

        # add searchform to global variable
        g.search_form = SearchForm()


# methods tells Flask that this view accepts both get and post
@app.route('/login', methods=['GET', 'POST'])
@oid.loginhandler
def login():
    # g is global for storing and sharing data during the request
    if g.user is not None and g.user.is_authenticated:
        # url_for gives the URL for given view function
        return redirect(url_for('index'))

    form = LoginForm()

    # this happens when submit button is hit
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for=['nickname', 'email'])

    return render_template('login.html',
                           title="Sign In",
                           form=form,
                           providers=app.config['OPENID_PROVIDERS'])


# resp argument contains information returned by OpenID provider
@oid.after_login
def after_login(resp):
    # email validation
    if resp.email is None or resp.email == "":
        flash('Invalid login. Please try again.')
        return redirect(url_for('login'))

    # search user by email
    user = User.query.filter_by(email=resp.email).first()

    # if email not found, this is a nwe user to be registered
    if user is None:
        nickname = resp.nickname

        # sometimes there is no nickname in OpenID
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]

        nickname = User.make_unique_nickname(nickname)

        # add new user to DB
        user = User(nickname=nickname, email=resp.email, role=ROLE_USER)
        db.session.add(user)
        db.session.commit()

        # make user a follower of himself
        db.session.add(user.follow(user))
        db.session.commit()

    # load remember_me boolean from session
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember=remember_me)

    # this is core functionality : flask redirects user request to login page if user is not yet
    # authenticated, and try to access @login_required protected view. Flask will store original link as
    # next page, and can be returned to it after completing login.
    #  but flask must now what view logs the users in, so we define that in __init__.py
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


# when the URL will be /user/wojtek, this will be called with nickname = 'wojtek'
@app.route('/user/<nickname>')
@app.route('/user/<nickname>/<int:page>')
@login_required
def user(nickname, page=1):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash("User " + nickname + " not found.")
        return redirect(url_for('index'))
    posts = user.get_sorted_posts().paginate(page, POSTS_PER_PAGE, False)
    return render_template('user.html',
                           user=user,
                           posts=posts)


@app.route('/user/<nickname>/cards')
@app.route('/user/<nickname>/cards/<int:page>')
@login_required
def user_cards(nickname, page=1):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash("User " + nickname + " not found.")
        return redirect(url_for('index'))

    from config import POSTS_PER_PAGE
    cards = user.get_cards().paginate(page, POSTS_PER_PAGE, False)

    if nickname == g.user.nickname:
        form = CardEditForm()
    else:
        form = CardReserveForm()

    return render_template('user_cards.html',
                           user=user,
                           cards=cards, form=form)


@app.route('/users/')
@login_required
def users(page=1):
    from config import USERS_PER_PAGE
    users = User.query.paginate(page, USERS_PER_PAGE, False)
    return render_template('users.html',
                           users=users)


@app.route('/reserved_cards/')
@login_required
def reserved_cards(page=1):
    from config import USERS_PER_PAGE
    query = Card.query.filter(Card.user_id != g.user.id).filter(Card.reserved > 0)
    cards = query.paginate(page, USERS_PER_PAGE, False)
    return render_template('reserved_cards.html',
                           cards=cards)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/user/<nickname>/add_new_card', methods=['GET', 'POST'])
@login_required
def add_card(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash("User " + nickname + " not found.")
        return redirect(url_for('index'))

    from forms import UploadForm
    form = UploadForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            image = form.image
            if image.data:
                filename = random_string(32)

                image_data = request.files[form.image.name].read()
                with open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'wb') as f:
                    f.write(image_data)

                card = Card(name=form.name.data, filename=filename, owner=user, quantity=form.quantity.data,
                            description=form.description.data, price=form.price.data)

                db.session.add(card)
                db.session.commit()
                flash(u'Dodano nową kartę.')
                return redirect(url_for('add_card',
                                        nickname=nickname))
    return render_template('add_card.html', form=form)


@app.route('/upload/<path:path>')
def serve_images(path):
    from flask import send_from_directory
    import config
    if not os.path.exists(os.path.join(config.UPLOAD_FOLDER, path)):
        return redirect(url_for('index'))
    return send_from_directory('upload', path)


@app.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditForm(g.user.nickname, lat=g.user.lat, lon=g.user.lon)

    if form.validate_on_submit():
        g.user.nickname = form.nickname.data
        g.user.about_me = form.about_me.data
        g.user.lat = form.lat.data
        g.user.lon = form.lon.data
        db.session.add(g.user)
        db.session.commit()
        flash('Your profile has been changed.')
        return redirect(url_for('edit'))
    else:
        form.nickname.data = g.user.nickname
        form.about_me.data = g.user.about_me
    return render_template('edit.html', form=form)


@app.route('/card_reserve/<nickname>/<card_id>', methods=['POST'])
@login_required
def card_reserve(nickname, card_id):
    form = CardReserveForm()

    if form.validate_on_submit():
        card = Card.query.filter_by(id=card_id).first()
        if form.unreserve.data is True:
            card.reserved = 0
            db.session.add(card)
            db.session.commit()
            flash(u'Usunięto rezerwacje karty.')
        else:
            cards_to_reserve = form.reserve.data
            if card.free_cards() >= cards_to_reserve:
                card.reserved += cards_to_reserve
                db.session.add(card)
                db.session.commit()
                flash(u'Zarezerwowano %s kart.' % cards_to_reserve)
            else:
                flash(u'Nie można zarezerwować takiej ilości kart.')
    return redirect(url_for('user_cards', nickname=nickname))


@app.route('/card_edit/<nickname>/<card_id>', methods=['POST'])
@login_required
def card_edit(nickname, card_id):
    form = CardEditForm()

    if form.validate_on_submit():
        card = Card.query.filter_by(id=card_id).first()
        if form.delete.data is True:
            if card.reserved == 0:
                db.session.delete(card)
                db.session.commit()
                flash(u'Usunięto kartę.')
            else:
                flash(u'Nie można usunąć karty na którą są już rezerwacje!')
        else:
            new_quantity = form.quantity.data
            if card.reserved > new_quantity:
                flash(u'Nie można zmienić ilości kart poniżej zarezerwowanej ilości.')
            else:
                card.quantity = new_quantity
                db.session.add(card)
                db.session.commit()
                flash(u'Zmieniono liczbę kart.')
    return redirect(url_for('user_cards', nickname=nickname))





@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    if user == g.user:
        flash('You cant follow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.follow(user)
    if u is None:
        flash('Cannot follow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('You are now following user ' + nickname + '!')

    follower_notification(user, g.user)

    return redirect(url_for('user', nickname=nickname))


@app.route('/unfollow/<nickname>')
@login_required
def unfollow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    if user == g.user:
        flash('You cant unfollow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.unfollow(user)
    if u is None:
        flash('Cannot unfollow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('You have stopped following user ' + nickname + '!')
    return redirect(url_for('user', nickname=nickname))


# checks if query is valid and redirect to results page
# this is done because when user hits refresh, query wont be resubmitted
@app.route('/search', methods=['POST'])
@login_required
def search():
    if not g.search_form.validate_on_submit():
        return redirect(url_for('index'))
    return redirect(url_for('search_results', query=g.search_form.search.data))


# this just calls for search query on whoosh search.db, and renders
# template with results
@app.route('/search_results/<query>')
@login_required
def search_results(query):
    results = Post.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
    return render_template('search_results.html', query=query, results=results)


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    print "Obsluguje"
    db.session.rollback()
    return render_template('500.html'), 500
