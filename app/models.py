from hashlib import md5

from app import db
from app import app

from sqlalchemy.sql import func

import flask.ext.whooshalchemy as whooshalchemy

ROLE_USER = 0
ROLE_ADMIN = 1

# many-to-many table with no associated model of its own
# many-to-many requires that table for easy lookups 
# for : list of followers of user1, list of users followed by user1
# SQLAlchemy deals with this table
followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id')),
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    nickname = db.Column(db.String(64), index = True, unique = True)
    email = db.Column(db.String(120), index = True, unique = True)
    role = db.Column(db.SmallInteger, default = ROLE_USER)
    lat = db.Column(db.Float, nullable=True)
    lon = db.Column(db.Float, nullable=True)

    # not an actual field, definied on `one` side in one to many relationship
    # arguments: 1) classname of `many` class 2) field that will be added to `many` class
    # that points back to `one` object

    # author will be 'virtual' field of each post, which will link to its author
    posts = db.relationship('Post', backref = 'author', lazy = 'dynamic')
    cards = db.relationship('Card', backref='owner', lazy='dynamic')

    about_me = db.Column(db.String(255))
    last_seen = db.Column(db.DateTime)

    # many-to-many relationship
    followed = db.relationship('User',
        # association table
        secondary = followers,
        # left-join
        primaryjoin = (followers.c.follower_id == id),
        # right-join
        secondaryjoin = (followers.c.followed_id == id),
        # reference which return all right-side users for specific
        # left-side user
        backref = db.backref('followers', lazy = 'dynamic'),
        lazy = 'dynamic')

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + \
        '?d=identicon&s=' + str(size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def get_sorted_posts(self):
        return self.posts.order_by(Post.date.desc())

    def get_cards(self):
        return self.cards.order_by(Card.name.asc())

    def get_card_count(self):
        card_count = Card.query.with_entities(func.sum(Card.quantity)).filter(Card.user_id==self.id).scalar()
        if not card_count:
            return 0
        return card_count

    def get_reserved_card_count(self):
        reserved_card_count = Card.query.with_entities(func.sum(Card.reserved)).filter(Card.user_id!=self.id).scalar()
        if not reserved_card_count:
            return 0
        return reserved_card_count

    def get_reserved_card_price(self):
        reserved_cards = Card.query.filter(Card.user_id != self.id).filter(Card.reserved > 0)
        sum = 0
        for card in reserved_cards:
            sum += card.reserved * card.price
        return sum


    def is_following(self, user):
        # followed is a relationship query which returns pairs (follower, followed)
        # because this is dynamic query object, we can filter it before execution
        # query executes when .count() is called
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):

        return Post.query.join(followers, (followers.c.followed_id == \
            Post.user_id)).filter(followers.c.follower_id == self.id).order_by(Post.date.desc())

    @staticmethod
    def make_unique_nickname(nickname):
        if User.query.filter_by(nickname = nickname).first() == None:
            return nickname
        version = 2
        while True:
            new_nickname = nickname + str(version)
            if User.query.filter_by(nickname = new_nickname).first() == None:
                break
            version += 1
        return new_nickname

    def __repr__(self):
        return '<User %r>' % (self.nickname)

class Post(db.Model):
    __searchable__ = ['body']

    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(255))
    date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


    def __repr__(self):
        return '<Post %r>' % (self.body)

class Card(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    description = db.Column(db.String(1000))
    filename = db.Column(db.String(100))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # card owner
    quantity = db.Column(db.Integer, default=1)
    reserved = db.Column(db.Integer, default=0)
    price = db.Column(db.Float, default=0.0)

    def __repr__(self):
        return '<Card %r>' % (self.name)

    def get_full_path(self):
        import os
        import config
        return os.path.join(config.UPLOAD_FOLDER, self.filename)

    def free_cards(self):
        return self.quantity - self.reserved


# class UserReservation(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#
#     user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # card reservee
#     owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # card owner
#     card_id = db.Column(db.Integer, db.ForeignKey('user.id'))


# initialised an index of Posts model in text-search db
whooshalchemy.whoosh_index(app, Post)
