from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail

# this is the init script
# 
# it creates app object and loads views module

import os

from flask.ext.login import LoginManager
from flask.ext.openid import OpenID

from config import basedir, ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD

app = Flask(__name__)

# load config from config.py
app.config.from_object('config')

db = SQLAlchemy(app)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

# openID extension requires a path to temp dir
oid = OpenID(app, os.path.join(basedir, 'tmp'))

# initialize mail object
mail = Mail(app)

# set up email error report when running in production mode
# also set up file logging 
if not app.debug:

    import logging
    from logging.handlers import SMTPHandler, RotatingFileHandler

    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)

    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + \
    	MAIL_SERVER, ADMINS, 'microblog failure', credentials)
    mail_handler.setLevel(logging.ERROR)

    # adds mail handler as error handler
    app.logger.addHandler(mail_handler)

    # limited size log, 1 Megabyte
    file_handler = RotatingFileHandler('tmp/blog.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: \
     %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)

    #adds log file handler as error handler
    app.logger.addHandler(file_handler)
    app.logger.info('blog startup')

from app import views, models
