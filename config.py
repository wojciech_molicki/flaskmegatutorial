import os

basedir = os.path.abspath(os.path.dirname(__file__))

# path to database file
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

# path for migrate data files
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

#cross site request forgery prevention
CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'

# mailserver settings for error reporting
MAIL_SERVER = 'smtp.googlemail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'placeholder_login'
MAIL_PASSWORD = 'placeholder_password'


UPLOAD_FOLDER = os.path.join(basedir, 'app', 'static', 'upload')
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# pagination conf
POSTS_PER_PAGE = 5
USERS_PER_PAGE = 15
COLUMNS_PER_PAGE = 3
ROWS_PER_PAGE = 5
CARDS_PER_PAGE = COLUMNS_PER_PAGE * ROWS_PER_PAGE

# administrator list
ADMINS = ['iamanadmin@example.com']

# text search db
WHOOSH_BASE = os.path.join(basedir, 'search.db')
MAX_SEARCH_RESULTS = 20

OPENID_PROVIDERS = [
    { 'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id' },
    { 'name': 'Yahoo', 'url': 'https://me.yahoo.com' },
    { 'name': 'AOL', 'url': 'http://openid.aol.com/<username>' },
    { 'name': 'Flickr', 'url': 'http://www.flickr.com/<username>' },
    { 'name': 'MyOpenID', 'url': 'https://www.myopenid.com' }]
