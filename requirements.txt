Flask
flask-login
flask-openid
flask-mail
sqlalchemy
flask-sqlalchemy
sqlalchemy-migrate
flask-whooshalchemy # is a full-text-search engine
flask-wtf # is a form validation tools
pytz
flask-babel
flup
