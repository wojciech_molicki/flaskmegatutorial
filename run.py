#!./env/bin/python

from app import app

# this is starting script

app.run(debug=True, host='0.0.0.0', port=8889)
